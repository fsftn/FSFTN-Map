var map = L.map('map', {
  center: [12.5, 80.0],
  minZoom: 4,
  zoom: 7
});
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a', 'b', 'c']
}).addTo(map);
for (var i = 0; i < markers.length; ++i) {
  L.marker([markers[i].lat, markers[i].lng])
    .bindPopup('<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a><br><p>This is the Description Area</p>')
    .addTo(map);
}
